import os,shutil

IMAGE_SIZE=(48,48)


def process_csv_file(filename):
	# process csv file and generate dlib points
	pass	
def image_to_csv(filename):
	pass

def images_to_csv(folder,output_file):
	with open(output_file,"w+") as output_f: 	
		for filename in os.listdir(folder):
			csv_=image_to_csv


def get_csv_from_microsoft_api_dataset(foldername):
	# convert the images into gray scale, resize and save into csv file
	#for file in os.listdir(foldername):
	print(len(os.listdir(foldername)))
def get_extension(filename):
	if(filename.count(".")==0) or filename.rindex(".")==len(filename)-1:
		return None
	else:
		return filename[filename.rindex(".")+1:]
def copy_labeled__from_microsoft_api_dataset(source_folder,dest_folder):
	for filename in os.listdir(os.path.join(source_folder,"microsoft_api")):
		if(get_extension(filename)=="json"):
			parts=filename.split(".")
			image_name=parts[0]+"."+parts[1]
			shutil.copyfile(os.path.join(source_folder,image_name),os.path.join(dest_folder,image_name))
def count_extension(folder,ext):
	count=0
	for filename in os.listdir(folder):
		e=get_extension(filename)
		if(e==ext):
			count+=1
	return count
def get_extensions(folder):
	output=set()
	for filename in os.listdir(folder):
		e=get_extension(filename)
		output.add(e)
	return output

if __name__=="__main__":
	# get_csv_from_microsoft_api_dataset("/home/mitiku/Desktop/set_10k")
	# copy_labeled__from_microsoft_api_dataset("/home/mitiku/Desktop/set_10k","/home/mitiku/iCog/datasets/microsoft_api")
	print count_extension("/home/mitiku/Desktop/set_10k","csv")
	# print get_extensions("/home/mitiku/Desktop/set_10k")