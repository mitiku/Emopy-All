import cv2
import numpy as np
import os
from sklearn.model_selection import train_test_split

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D

# Config-Vars
IMG_SIZE = (48, 48)
BATCH_SIZE = 64
EPOCHS = 80
EMOTIONS = ['anger', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']


# Misc Functions
def sanitize(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to grayscale
    img = cv2.resize(img, IMG_SIZE)  # Resize
    return img


def emotion_to_string(emotion):
    return EMOTIONS[emotion]


def string_to_emotion(string):
    return EMOTIONS.index(string)


def load_dataset(directory):
    x, y = [], []

    # Read images from the directory
    for emotion_dir in os.listdir(directory):
        for filename in os.listdir(os.path.join(directory, emotion_dir)):
            x += [sanitize(cv2.imread(os.path.join(directory, emotion_dir, filename)))]
            y += [string_to_emotion(emotion_dir)]

    # Convert to numpy array
    x = np.array(x, dtype='uint8')
    y = np.array(y)

    return x, y


def binarize(x, y, emotion):
    positives = (y == emotion)
    xp = x[positives]

    xn = x[np.invert(positives)]
    xn = xn[np.random.choice(xn.shape[0], xp.shape[0], replace=False)]

    x = np.concatenate((xp, xn), axis=0)
    y = np.concatenate((np.ones(xp.shape[0], dtype=np.int), np.zeros(xn.shape[0], dtype=np.int)), axis=0)
    y = np.eye(2)[y]

    return x, y


def main():
    x_all, y_all = load_dataset("dataset")

    for i in range(7):
        print "Modeling ", emotion_to_string(i)

        x, y = binarize(x_all, y_all, i)
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

        x_train = x_train.reshape(x_train.shape[0], IMG_SIZE[0], IMG_SIZE[1], 1)
        x_test = x_test.reshape(x_test.shape[0], IMG_SIZE[0], IMG_SIZE[1], 1)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255

        model = Sequential()
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(IMG_SIZE[0], IMG_SIZE[1], 1)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, (3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))
        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        model.add(Dropout(0.2))
        model.add(Dense(2, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.Adadelta(),
                      metrics=['accuracy'])

        model.fit(x_train, y_train,
                  batch_size=BATCH_SIZE,
                  epochs=EPOCHS,
                  verbose=1,
                  validation_data=(x_test, y_test))

        # Save model to file
        model_json = model.to_json()
        with open("models/" + emotion_to_string(i) + ".json", "w") as json_file:
            json_file.write(model_json)
        model.save_weights("models/" + emotion_to_string(i) + ".h5")
        print("Saved model to disk")


# Main
main()