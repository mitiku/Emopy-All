
#include "CameraDetector.h"
#include <iostream>

using namespace std;

class Listener : public affdex::FaceListener{
	void onFaceFound(float timestamp, affdex::FaceId faceId){
		cout<<"Hi "<<faceId<<endl;
	}
	void onFaceLost(float timestamp, affdex::FaceId faceId){
		cout<<"Bye "<<faceId<<endl;
	}
};


int main(int argc, char const *argv[])
{
	/* code */
	affdex::CameraDetector detector;
	detector.setClassifierPath("/home/mitiku/affdex-sdk/data");
	detector.setDetectAllEmotions(true);
	std::shared_ptr<affdex::FaceListener> listener(new Listener());
	detector.setFaceListener(listener.get());
	detector.start();
	int x;
	cin>>x;
	detector.stop();
	return 0;
}