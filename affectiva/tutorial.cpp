
#include "PhotoDetector.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

class Listener : public affdex::ImageListener{
	void onImageResults(std::map<affdex::FaceId,affdex::Face> faces,affdex::Frame image){
		std::string pronoun="they";
		std::string emotion="neutral";
		cout<<"onImageResults"<<endl;
		for (auto pair : faces){
			affdex::FaceId faceId=pair.first;
			affdex::Face face=pair.second;
			if(face.appearance.gender==affdex::Gender::Male){
				pronoun="Male";
			}else if(face.appearance.gender==affdex::Gender::Female){
				pronoun="Female";
			}

			if(face.emotions.joy>25){
				emotion="Happy :)";
			}else if(face.emotions.surprise>25){
				emotion="Surprise D:)";
			}
			cout<<"Joy: "<<face.emotions.joy<<endl;
			// cout<<"Smile: "<<face.emotions.smile<<endl;
			cout<<"saddness: "<<face.emotions.sadness<<endl;
			// cout<<"normal: "<<face.emotions.normal<<endl;
			cout<<"anger: "<<face.emotions.anger<<endl;
			cout<<"disgust: "<<face.emotions.disgust<<endl;
			cout<<"contempt: "<<face.emotions.contempt<<endl;
			cout<<"Surprise "<<face.emotions.surprise<<endl;
			cout<<faceId<<" : "<<pronoun <<" looks "<< emotion <<endl;
		}

	}
	void onImageCapture(affdex::Frame image){
		cout<<"IMage captured"<<endl;
	}
};


int main(int argc, char const *argv[])
{
	/* code */
	Mat img;
	img=imread(argv[1],1);
	affdex::Frame frame(img.size().width, img.size().height, img.data, affdex::Frame::COLOR_FORMAT::BGR);
	affdex::PhotoDetector detector(3);
	detector.setClassifierPath("/home/mitiku/affdex-sdk/data");
	affdex::ImageListener * listener(new Listener());
	detector.setImageListener(listener);
	detector.setDetectAllEmotions(true);
	detector.setDetectAllExpressions(true);
	detector.start();
	detector.process(frame);
	detector.stop();
	return 0;
}